# netboxr #

An R package to mark functional modules in the network analysis using data from pathwayCommon 2 and TCGA firehose.

* Version: 1.0

### Installing netboxr ###

* Quick summary
```
library(devtools)
install_bitbucket(repo="mil2041/netboxr",ref="master",build_vignette=TRUE)
```